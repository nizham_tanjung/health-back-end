package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.Pasien;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepoPasien extends CrudRepository<Pasien, Long> {
    public List<Pasien> findByNamaPasienLike(String nama);

    public List<Pasien> findByTelpon2(String phone);
}
