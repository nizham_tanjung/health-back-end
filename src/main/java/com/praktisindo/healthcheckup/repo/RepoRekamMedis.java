package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.RekamMedis;
import org.springframework.data.repository.CrudRepository;

public interface RepoRekamMedis extends CrudRepository<RekamMedis, Long> {
}
