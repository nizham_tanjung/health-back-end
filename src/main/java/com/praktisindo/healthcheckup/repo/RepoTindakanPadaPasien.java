package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.TindakanPadaPasien;
import org.springframework.data.repository.CrudRepository;

public interface RepoTindakanPadaPasien extends CrudRepository<TindakanPadaPasien, Long> {
}
