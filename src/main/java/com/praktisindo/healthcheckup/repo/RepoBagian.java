package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.Bagian;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepoBagian extends CrudRepository<Bagian, Long> {
    public List<Bagian> findByIdPoli(Long idPoli);
}
