package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.TenagaMedis;
import org.springframework.data.repository.CrudRepository;

public interface RepoTenagaMedis extends CrudRepository<TenagaMedis, Long> {
    Iterable<Object> findByNamaLikeIgnoreCase(String name);
}
