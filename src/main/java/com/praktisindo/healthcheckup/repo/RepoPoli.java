package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.Poli;
import org.springframework.data.repository.CrudRepository;

public interface RepoPoli extends CrudRepository<Poli, Long> {
}
