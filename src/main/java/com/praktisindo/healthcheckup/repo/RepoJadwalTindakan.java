package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.JadwalTindakan;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface RepoJadwalTindakan extends CrudRepository<JadwalTindakan, Long> {
    List<JadwalTindakan> findByTanggal(LocalDate tanggal);
}
