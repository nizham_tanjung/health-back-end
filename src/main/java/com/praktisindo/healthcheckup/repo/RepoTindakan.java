package com.praktisindo.healthcheckup.repo;

import com.praktisindo.healthcheckup.domain.Tindakan;
import org.springframework.data.repository.CrudRepository;

public interface RepoTindakan extends CrudRepository<Tindakan, Long> {
    public Iterable<Object> findByNamaTindakanLikeIgnoreCase(String name);
}
