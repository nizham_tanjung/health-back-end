package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.TenagaMedis;
import com.praktisindo.healthcheckup.domain.Tindakan;
import com.praktisindo.healthcheckup.repo.RepoTindakan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TindakanService {
    Logger logger = LoggerFactory.getLogger(TindakanService.class);

    @Autowired
    private RepoTindakan repoTindakan;

    public List<Tindakan> semuaTindakan() {
        List<Tindakan> tindakans = new ArrayList<>();
        if (repoTindakan.count() > 0) {
            repoTindakan.findAll().forEach(t -> {
                tindakans.add(t);
            });
        }
        return tindakans;
    }

    public Optional<Tindakan> satuTindakan(Long id) {
        return repoTindakan.findById(id);
    }

    public String simpan(Tindakan tindakan) {
        try {
            repoTindakan.save(tindakan);
            logger.info("Berhasil menyimpan tindakan " + tindakan.getNamaTindakan());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan tindakan. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(Tindakan tindakan) {
        try {
            repoTindakan.delete(tindakan);
            logger.info("Berhasil menghapus tindakan " + tindakan.getNamaTindakan());
            return "Berhasil menhapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menhapus tindakan. err: " + e.getMessage());
            return "Tidak berhasil menhapus";
        }
    }

    public List<Tindakan> tindakanByName(String name) {
        List<Tindakan> tindakanList = new ArrayList<>();
        if (repoTindakan.count() > 0) {
            repoTindakan.findByNamaTindakanLikeIgnoreCase(name).forEach(r -> {
                tindakanList.add((Tindakan) r);
            });
        }
        return tindakanList;
    }
}
