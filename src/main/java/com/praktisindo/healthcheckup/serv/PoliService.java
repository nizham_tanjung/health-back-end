package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.Poli;
import com.praktisindo.healthcheckup.repo.RepoPoli;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PoliService {
    Logger logger = LoggerFactory.getLogger(PoliService.class);

    @Autowired
    private RepoPoli repoPoli;

    public List<Poli> semuaPoli() {
        List<Poli> Polis = new ArrayList<>();
        if (repoPoli.count() > 0) {
            repoPoli.findAll().forEach(b -> {
                Polis.add(b);
            });
        }
        return Polis;
    }

    public Optional<Poli> satuPoli(Long id) {
        return repoPoli.findById(id);
    }

    public String simpan(Poli Poli) {
        try {
            repoPoli.save(Poli);
            logger.info("Berhasil menyimpan Poli " + Poli.getNamaPoli());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan Poli. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(Poli Poli) {
        try {
            repoPoli.delete(Poli);
            logger.info("Berhasil menghapus Poli " + Poli.getNamaPoli());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus Poli. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }
}
