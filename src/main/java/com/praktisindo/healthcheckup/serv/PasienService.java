package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.Pasien;
import com.praktisindo.healthcheckup.repo.RepoPasien;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PasienService {
    Logger logger = LoggerFactory.getLogger(PasienService.class);

    @Autowired
    private RepoPasien repoPasien;

    public List<Pasien> semuaPasien() {
        List<Pasien> Pasiens = new ArrayList<>();
        if (repoPasien.count() > 0) {
            repoPasien.findAll().forEach(b -> {
                Pasiens.add(b);
            });
        }
        return Pasiens;
    }

    public Optional<Pasien> satuPasien(Long id) {
        return repoPasien.findById(id);
    }

    public String simpan(Pasien Pasien) {
        try {
            repoPasien.save(Pasien);
            logger.info("Berhasil menyimpan Pasien " + Pasien.getNamaPasien());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan Pasien. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(Pasien Pasien) {
        try {
            repoPasien.delete(Pasien);
            logger.info("Berhasil menghapus Pasien " + Pasien.getNamaPasien());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus Pasien. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }

    public List<Pasien> patientByName(String nama) {
        List<Pasien> Pasiens = new ArrayList<>();
        if (repoPasien.count() > 0) {
            repoPasien.findByNamaPasienLike(nama).forEach(b -> {
                Pasiens.add(b);
            });
        }
        return Pasiens;
    }

    public List<Pasien> patientByPhone(String phone) {
        List<Pasien> Pasiens = new ArrayList<>();
        if (repoPasien.count() > 0) {
            repoPasien.findByTelpon2(phone).forEach(b -> {
                Pasiens.add(b);
            });
        }
        return Pasiens;
    }
}
