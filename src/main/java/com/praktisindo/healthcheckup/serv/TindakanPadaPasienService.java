package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.TindakanPadaPasien;
import com.praktisindo.healthcheckup.repo.RepoTindakanPadaPasien;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TindakanPadaPasienService {
    Logger logger = LoggerFactory.getLogger(TindakanPadaPasienService.class);

    @Autowired
    private RepoTindakanPadaPasien repoTPP;

    public List<TindakanPadaPasien> semuaTindakanPP() {
        List<TindakanPadaPasien> TindakanPadaPasiens = new ArrayList<>();
        if (repoTPP.count() > 0) {
            repoTPP.findAll().forEach(b -> {
                TindakanPadaPasiens.add(b);
            });
        }
        return TindakanPadaPasiens;
    }

    public Optional<TindakanPadaPasien> satuTindakanPP(Long id) {
        return repoTPP.findById(id);
    }

    public String simpan(TindakanPadaPasien TindakanPadaPasien) {
        try {
            repoTPP.save(TindakanPadaPasien);
            logger.info("Berhasil menyimpan TindakanPadaPasien " + TindakanPadaPasien.getNoRM());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan TindakanPadaPasien. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(TindakanPadaPasien TindakanPadaPasien) {
        try {
            repoTPP.delete(TindakanPadaPasien);
            logger.info("Berhasil menghapus TindakanPadaPasien " + TindakanPadaPasien.getNoRM());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus TindakanPadaPasien. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }
}
