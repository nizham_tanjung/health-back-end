package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.RekamMedis;
import com.praktisindo.healthcheckup.repo.RepoRekamMedis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RekamMedisService {
    Logger logger = LoggerFactory.getLogger(RekamMedisService.class);

    @Autowired
    private RepoRekamMedis repoRekamMedis;

    public List<RekamMedis> semuaRekamMedis() {
        List<RekamMedis> RekamMediss = new ArrayList<>();
        if (repoRekamMedis.count() > 0) {
            repoRekamMedis.findAll().forEach(b -> {
                RekamMediss.add(b);
            });
        }
        return RekamMediss;
    }

    public Optional<RekamMedis> satuRekamMedis(Long id) {
        return repoRekamMedis.findById(id);
    }

    public String simpan(RekamMedis RekamMedis) {
        try {
            repoRekamMedis.save(RekamMedis);
            logger.info("Berhasil menyimpan RekamMedis " + RekamMedis.getNoRM());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan RekamMedis. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(RekamMedis RekamMedis) {
        try {
            repoRekamMedis.delete(RekamMedis);
            logger.info("Berhasil menghapus RekamMedis " + RekamMedis.getNoRM());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus RekamMedis. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }
}
