package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.JadwalTindakan;
import com.praktisindo.healthcheckup.repo.RepoJadwalTindakan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JadwalTindakanService {
    Logger logger = LoggerFactory.getLogger(JadwalTindakanService.class);

    @Autowired
    private RepoJadwalTindakan repoJadwalTindakan;

    public List<JadwalTindakan> semuaJadwalTindakan() {
        List<JadwalTindakan> JadwalTindakans = new ArrayList<>();
        if (repoJadwalTindakan.count() > 0) {
            repoJadwalTindakan.findAll().forEach(b -> {
                JadwalTindakans.add(b);
            });
        }
        return JadwalTindakans;
    }

    public Optional<JadwalTindakan> satuJadwalTindakan(Long id) {
        return repoJadwalTindakan.findById(id);
    }

    public List<JadwalTindakan> JadwalTindakanPerTanggal(LocalDate tanggal) {
        try {
            return repoJadwalTindakan.findByTanggal(tanggal);
        } catch (Exception e) {
            logger.error("Tidak ada JadwalTindakan");
            List<JadwalTindakan> b = new ArrayList<>();
            return b;
        }
    }

    public String simpan(JadwalTindakan JadwalTindakan) {
        try {
            repoJadwalTindakan.save(JadwalTindakan);
            logger.info("Berhasil menyimpan JadwalTindakan " + JadwalTindakan.getTanggal());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan JadwalTindakan. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(JadwalTindakan JadwalTindakan) {
        try {
            repoJadwalTindakan.delete(JadwalTindakan);
            logger.info("Berhasil menghapus JadwalTindakan " + JadwalTindakan.getTanggal());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus JadwalTindakan. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }
}
