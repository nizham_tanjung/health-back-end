package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.Bagian;
import com.praktisindo.healthcheckup.repo.RepoBagian;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BagianService {
    Logger logger = LoggerFactory.getLogger(BagianService.class);

    @Autowired
    private RepoBagian repoBagian;

    public List<Bagian> semuaBagian() {
        List<Bagian> bagians = new ArrayList<>();
        if (repoBagian.count() > 0) {
            repoBagian.findAll().forEach(b -> {
                bagians.add(b);
            });
        }
        return bagians;
    }

    public Optional<Bagian> satuBagian(Long id) {
        return repoBagian.findById(id);
    }

    public List<Bagian> bagianPerPoli(Long poli) {
        try {
            return repoBagian.findByIdPoli(poli);
        } catch (Exception e) {
            logger.error("Tidak ada bagian dengan id poli " + poli);
            List<Bagian> b = new ArrayList<>();
            return b;
        }
    }

    public String simpan(Bagian bagian) {
        try {
            repoBagian.save(bagian);
            logger.info("Berhasil menyimpan bagian " + bagian.getNamaBagian() + " di poli " + bagian.getIdPoli());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan bagian. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(Bagian bagian) {
        try {
            repoBagian.delete(bagian);
            logger.info("Berhasil menghapus bagian " + bagian.getNamaBagian() + " di poli " + bagian.getIdPoli());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus bagian. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }
}
