package com.praktisindo.healthcheckup.serv;

import com.praktisindo.healthcheckup.domain.Pasien;
import com.praktisindo.healthcheckup.domain.TenagaMedis;
import com.praktisindo.healthcheckup.repo.RepoTenagaMedis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TenagaMedisService {
    Logger logger = LoggerFactory.getLogger(TenagaMedisService.class);

    @Autowired
    private RepoTenagaMedis repoTenagaMedis;

    public List<TenagaMedis> semuaTenagaMedis() {
        List<TenagaMedis> TenagaMediss = new ArrayList<>();
        if (repoTenagaMedis.count() > 0) {
            repoTenagaMedis.findAll().forEach(b -> {
                TenagaMediss.add(b);
            });
        }
        return TenagaMediss;
    }

    public Optional<TenagaMedis> satuTenagaMedis(Long id) {
        return repoTenagaMedis.findById(id);
    }

    public String simpan(TenagaMedis TenagaMedis) {
        try {
            repoTenagaMedis.save(TenagaMedis);
            logger.info("Berhasil menyimpan TenagaMedis " + TenagaMedis.getNama());
            return "Berhasil menyimpan";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menyimpan TenagaMedis. err: " + e.getMessage());
            return "Tidak berhasil menyimpan";
        }
    }

    public String hapus(TenagaMedis TenagaMedis) {
        try {
            repoTenagaMedis.delete(TenagaMedis);
            logger.info("Berhasil menghapus TenagaMedis " + TenagaMedis.getNama());
            return "Berhasil menghapus";
        } catch (Exception e) {
            System.err.println("Tidak berhasil menghapus TenagaMedis. err: " + e.getMessage());
            return "Tidak berhasil menghapus";
        }
    }

    public List<TenagaMedis> medicByName(String name) {
        List<TenagaMedis> tenagaMedisList = new ArrayList<>();
        if (repoTenagaMedis.count() > 0) {
            repoTenagaMedis.findByNamaLikeIgnoreCase(name).forEach(r -> {
                tenagaMedisList.add((TenagaMedis) r);
            });
        }
        return tenagaMedisList;
    }
}
