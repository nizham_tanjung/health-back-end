package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.TindakanPadaPasien;
import com.praktisindo.healthcheckup.serv.TindakanPadaPasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tpp")
@CrossOrigin("http://localhost:8081")
public class TindakanPadaPasienCtrl {

    @Autowired
    private TindakanPadaPasienService tindakanService;

    @GetMapping("/")
    public ResponseEntity<List<TindakanPadaPasien>> daftarTindakan() {
        List<TindakanPadaPasien> padaPasienList = tindakanService.semuaTindakanPP();
        if (padaPasienList.isEmpty()) {
            return new ResponseEntity<>(padaPasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(padaPasienList, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<TindakanPadaPasien> ambilTindakan(@PathVariable Long id) {
        if (tindakanService.satuTindakanPP(id).isPresent()) {
            TindakanPadaPasien tindakan = tindakanService.satuTindakanPP(id).get();
            return new ResponseEntity<>(tindakan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> simpanTindakan(@RequestBody TindakanPadaPasien tindakan) {
        return new ResponseEntity<>(tindakanService.simpan(tindakan), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateTindakan(@RequestBody TindakanPadaPasien tindakan) {
        return new ResponseEntity<>(tindakanService.simpan(tindakan), HttpStatus.OK);
    }

    @DeleteMapping("/del")
    public ResponseEntity<String> hapusTindakan(@RequestBody TindakanPadaPasien tindakan) {
        return new ResponseEntity<>(tindakanService.hapus(tindakan), HttpStatus.OK);
    }

}
