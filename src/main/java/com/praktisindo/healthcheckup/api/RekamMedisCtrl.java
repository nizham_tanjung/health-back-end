package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.RekamMedis;
import com.praktisindo.healthcheckup.serv.RekamMedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rm")
@CrossOrigin("http://localhost:8081")
public class RekamMedisCtrl {

    @Autowired
    private RekamMedisService rmService;

    @GetMapping("/")
    public ResponseEntity<List<RekamMedis>> daftarPasien() {
        List<RekamMedis> rmList = rmService.semuaRekamMedis();
        if (rmList.isEmpty()) {
            return new ResponseEntity<>(rmList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(rmList, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<RekamMedis> ambilRekamMedis(@PathVariable Long id) {
        if (rmService.satuRekamMedis(id).isPresent()) {
            RekamMedis rekamMedis = rmService.satuRekamMedis(id).get();
            return new ResponseEntity<>(rekamMedis, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> simpanRekamMedis(@RequestBody RekamMedis rekamMedis) {
        return new ResponseEntity<>(rmService.simpan(rekamMedis), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateRekamMedis(@RequestBody RekamMedis rekamMedis) {
        return new ResponseEntity<>(rmService.simpan(rekamMedis), HttpStatus.OK);
    }

    @DeleteMapping("/del")
    public ResponseEntity<String> hapusRekamMedis(@RequestBody RekamMedis rekamMedis) {
        return new ResponseEntity<>(rmService.hapus(rekamMedis), HttpStatus.OK);
    }
    
}
