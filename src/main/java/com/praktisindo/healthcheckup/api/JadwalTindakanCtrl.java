package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.JadwalTindakan;
import com.praktisindo.healthcheckup.serv.JadwalTindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/jadwal")
@CrossOrigin("http://localhost:8081")
public class JadwalTindakanCtrl {

    @Autowired
    private JadwalTindakanService jadwalService;

    @GetMapping("/")
    public ResponseEntity<List<JadwalTindakan>> daftarJadwalTindakan() {
        List<JadwalTindakan> jadwalTindakanList = jadwalService.semuaJadwalTindakan();
        if (jadwalTindakanList.isEmpty()) {
            return new ResponseEntity<>(jadwalTindakanList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(jadwalTindakanList, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<JadwalTindakan> ambilJadwalTindakan(@PathVariable Long id) {
        if (jadwalService.satuJadwalTindakan(id).isPresent()) {
            JadwalTindakan jadwal = jadwalService.satuJadwalTindakan(id).get();
            return new ResponseEntity<>(jadwal, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> simpanJadwalTindakan(@RequestBody JadwalTindakan jadwal) {
        return new ResponseEntity<>(jadwalService.simpan(jadwal), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateJadwalTindakan(@RequestBody JadwalTindakan jadwal) {
        return new ResponseEntity<>(jadwalService.simpan(jadwal), HttpStatus.OK);
    }

    @DeleteMapping("/del")
    public ResponseEntity<String> hapusJadwalTindakan(@RequestBody JadwalTindakan jadwal) {
        return new ResponseEntity<>(jadwalService.hapus(jadwal), HttpStatus.OK);
    }

}
