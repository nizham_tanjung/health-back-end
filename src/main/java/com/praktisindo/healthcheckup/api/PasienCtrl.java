package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.Pasien;
import com.praktisindo.healthcheckup.serv.PasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081")
public class PasienCtrl {
    
    @Autowired
    private PasienService pasienService;

    @GetMapping("/pasien")
    public ResponseEntity<List<Pasien>> daftarPasien() {
        List<Pasien> pasienList = pasienService.semuaPasien();
        if (pasienList.isEmpty()) {
            return new ResponseEntity<>(pasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pasienList, HttpStatus.OK);
        }
    }

    @GetMapping("/pasien/{id}")
    public ResponseEntity<Pasien> ambilPasien(@PathVariable Long id) {
        if (pasienService.satuPasien(id).isPresent()) {
            Pasien p = pasienService.satuPasien(id).get();
            return new ResponseEntity<>(p, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/pasien/add")
    public ResponseEntity<String> simpanPasien(@RequestBody Pasien Pasien) {
        return new ResponseEntity<>(pasienService.simpan(Pasien), HttpStatus.OK);
    }

    @PutMapping("/pasien/update")
    public ResponseEntity<String> updatePasien(@RequestBody Pasien Pasien) {
        return new ResponseEntity<>(pasienService.simpan(Pasien), HttpStatus.OK);
    }

    @DeleteMapping("/pasien/del/{id}")
    public ResponseEntity<String> hapusPasien(@PathVariable Long id) {
        Pasien p = pasienService.satuPasien(id).get();
        return new ResponseEntity<>(pasienService.hapus(p), HttpStatus.OK);
    }

    @GetMapping("/pasien/search/by_name/{name}")
    public ResponseEntity<List<Pasien>> patientByName(@PathVariable String name) {
        List<Pasien> pasienList = pasienService.patientByName(name);
        if (pasienList.isEmpty()) {
            return new ResponseEntity<>(pasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pasienList, HttpStatus.OK);
        }
    }

    @GetMapping("/pasien/search/by_phone/{phone}")
    public ResponseEntity<List<Pasien>> patientByPhone(@PathVariable String phone) {
        List<Pasien> pasienList = pasienService.patientByPhone(phone);
        if (pasienList.isEmpty()) {
            return new ResponseEntity<>(pasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pasienList, HttpStatus.OK);
        }
    }
}
