package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.Bagian;
import com.praktisindo.healthcheckup.domain.Poli;
import com.praktisindo.healthcheckup.serv.BagianService;
import com.praktisindo.healthcheckup.serv.PoliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081")
public class PoliCtrl {

    @Autowired
    private PoliService poliService;

    @Autowired
    private BagianService bagianService;

    // Poli
    @GetMapping("/poli")
    public ResponseEntity<List<Poli>> daftarPoli() {
        List<Poli> poliList = poliService.semuaPoli();
        if (poliList.isEmpty()) {
            return new ResponseEntity<>(poliList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(poliList, HttpStatus.OK);
        }
    }

    @GetMapping("/poli/{id}")
    public ResponseEntity<Poli> ambilPoli(@PathVariable Long id) {
        if (poliService.satuPoli(id).isPresent()) {
            Poli poli = poliService.satuPoli(id).get();
            return new ResponseEntity<>(poli, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/poli/add")
    public ResponseEntity<String> simpanPoli(@RequestBody Poli poli) {
        return new ResponseEntity<>(poliService.simpan(poli), HttpStatus.OK);
    }

    @PutMapping("/poli/update")
    public ResponseEntity<String> updatePoli(@RequestBody Poli poli) {
        return new ResponseEntity<>(poliService.simpan(poli), HttpStatus.OK);
    }

    @DeleteMapping("/poli/del/{id}")
    public ResponseEntity<String> hapusPoli(@PathVariable Long id) {
        Poli poli = poliService.satuPoli(id).get();
        return new ResponseEntity<>(poliService.hapus(poli), HttpStatus.OK);
    }

    // Bagian
    @GetMapping("/bagian/poli/{poliId}")
    public ResponseEntity<List<Bagian>> bagianPerPoli(Long poliId) {
        return new ResponseEntity<>(bagianService.bagianPerPoli(poliId), HttpStatus.OK);
    }

    @GetMapping("/bagian")
    public ResponseEntity<List<Bagian>> daftarBagian() {
        List<Bagian> bagianList = bagianService.semuaBagian();
        if (bagianList.isEmpty()) {
            return new ResponseEntity<>(bagianList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(bagianList, HttpStatus.OK);
        }
    }

    @GetMapping("/bagian/{id}")
    public ResponseEntity<Bagian> ambilBagian(@PathVariable Long id) {
        if (bagianService.satuBagian(id).isPresent()) {
            Bagian bagian = bagianService.satuBagian(id).get();
            return new ResponseEntity<>(bagian, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/bagian/add")
    public ResponseEntity<String> simpanBagian(@RequestBody Bagian bagian) {
        return new ResponseEntity<>(bagianService.simpan(bagian), HttpStatus.OK);
    }

    @PutMapping("/bagian/update")
    public ResponseEntity<String> updateBagian(@RequestBody Bagian bagian) {
        return new ResponseEntity<>(bagianService.simpan(bagian), HttpStatus.OK);
    }

    @DeleteMapping("/bagian/del/{id}")
    public ResponseEntity<String> hapusBagian(@PathVariable Long id) {
        Bagian bagian = bagianService.satuBagian(id).get();
        return new ResponseEntity<>(bagianService.hapus(bagian), HttpStatus.OK);
    }
}
