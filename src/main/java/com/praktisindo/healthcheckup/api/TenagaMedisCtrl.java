package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.TenagaMedis;
import com.praktisindo.healthcheckup.serv.TenagaMedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081")
public class TenagaMedisCtrl {
    @Autowired
    private TenagaMedisService tenagaMedisService;

    @GetMapping("/tenagamedis")
    public ResponseEntity<List<TenagaMedis>> daftarTenagaMedis() {
        List<TenagaMedis> tenagaMedisList = tenagaMedisService.semuaTenagaMedis();
        if (tenagaMedisList.isEmpty()) {
            return new ResponseEntity<>(tenagaMedisList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(tenagaMedisList, HttpStatus.OK);
        }
    }

    @GetMapping("/tenagamedis/{id}")
    public ResponseEntity<TenagaMedis> satuTenagaMedis(@PathVariable Long id) {
        if (tenagaMedisService.satuTenagaMedis(id).isPresent()) {
            TenagaMedis tenagaMedis = tenagaMedisService.satuTenagaMedis(id).get();
            return new ResponseEntity<>(tenagaMedis, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/tenagamedis/add")
    public ResponseEntity<String> simpanTindakan(@RequestBody TenagaMedis tenagaMedis) {
        return new ResponseEntity<>(tenagaMedisService.simpan(tenagaMedis), HttpStatus.OK);
    }

    @PutMapping("/tenagamedis/update")
    public ResponseEntity<String> updateTindakan(@RequestBody TenagaMedis tenagaMedis) {
        return new ResponseEntity<>(tenagaMedisService.simpan(tenagaMedis), HttpStatus.OK);
    }

    @DeleteMapping("/tenagamedis/del/{id}")
    public ResponseEntity<String> hapusTindakan(@PathVariable Long id) {
        TenagaMedis tenagaMedis = tenagaMedisService.satuTenagaMedis(id).get();
        return new ResponseEntity<>(tenagaMedisService.hapus(tenagaMedis), HttpStatus.OK);
    }

    @GetMapping("/tenagamedis/search/by_name/{name}")
    public ResponseEntity<List<TenagaMedis>> medicByName(@PathVariable String name) {
        List<TenagaMedis> tenagaMedisList = tenagaMedisService.medicByName(name);
        if (tenagaMedisList.isEmpty()) {
            return new ResponseEntity<>(tenagaMedisList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(tenagaMedisList, HttpStatus.OK);
        }
    }
}
