package com.praktisindo.healthcheckup.api;

import com.praktisindo.healthcheckup.domain.TenagaMedis;
import com.praktisindo.healthcheckup.domain.Tindakan;
import com.praktisindo.healthcheckup.serv.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081")
public class TindakanCtrl {

    @Autowired
    private TindakanService tindakanService;

    @GetMapping("/tindakan")
    public ResponseEntity<List<Tindakan>> daftarTindakan() {
        List<Tindakan> pasienList = tindakanService.semuaTindakan();
        if (pasienList.isEmpty()) {
            return new ResponseEntity<>(pasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pasienList, HttpStatus.OK);
        }
    }

    @GetMapping("/tindakan/{id}")
    public ResponseEntity<Tindakan> ambilTindakan(@PathVariable Long id) {
        if (tindakanService.satuTindakan(id).isPresent()) {
            Tindakan tindakan = tindakanService.satuTindakan(id).get();
            return new ResponseEntity<>(tindakan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/tindakan/add")
    public ResponseEntity<String> simpanTindakan(@RequestBody Tindakan tindakan) {
        return new ResponseEntity<>(tindakanService.simpan(tindakan), HttpStatus.OK);
    }

    @PutMapping("/tindakan/update")
    public ResponseEntity<String> updateTindakan(@RequestBody Tindakan tindakan) {
        return new ResponseEntity<>(tindakanService.simpan(tindakan), HttpStatus.OK);
    }

    @DeleteMapping("/tindakan/del/{id}")
    public ResponseEntity<String> hapusTindakan(@PathVariable Long id) {
        Tindakan tindakan = tindakanService.satuTindakan(id).get();
        return new ResponseEntity<>(tindakanService.hapus(tindakan), HttpStatus.OK);
    }

    @GetMapping("/tindakan/search/by_name/{name}")
    public ResponseEntity<List<Tindakan>> tindakanByName(@PathVariable String name) {
        List<Tindakan> tindakanList = tindakanService.tindakanByName(name);
        if (tindakanList.isEmpty()) {
            return new ResponseEntity<>(tindakanList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(tindakanList, HttpStatus.OK);
        }
    }
    
}
