package com.praktisindo.healthcheckup.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class TenagaMedis implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nama;

    private String alamat;

    private String telpon1;

    private String telpon2;

    private String email;

    private String posisi;

    private Long poli;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelpon1() {
        return telpon1;
    }

    public void setTelpon1(String telpon1) {
        this.telpon1 = telpon1;
    }

    public String getTelpon2() {
        return telpon2;
    }

    public void setTelpon2(String telpon2) {
        this.telpon2 = telpon2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public Long getPoli() {
        return poli;
    }

    public void setPoli(Long poli) {
        this.poli = poli;
    }
}
