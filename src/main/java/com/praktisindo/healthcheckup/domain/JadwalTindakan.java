package com.praktisindo.healthcheckup.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class JadwalTindakan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idPasien;

    private String namaPasien;

    private String pangkatPasien;

    private Long idTindakan;

    private String tindakan;

    private String operator; // Doctor

    private String noTelpPasien;

    private String keterangan;

    private LocalDate tanggal;

    private int queueNum;

    private boolean visit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdPasien() {
        return idPasien;
    }

    public void setIdPasien(Long idPasien) {
        this.idPasien = idPasien;
    }

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getPangkatPasien() {
        return pangkatPasien;
    }

    public void setPangkatPasien(String pangkatPasien) {
        this.pangkatPasien = pangkatPasien;
    }

    public Long getIdTindakan() {
        return idTindakan;
    }

    public void setIdTindakan(Long idTindakan) {
        this.idTindakan = idTindakan;
    }

    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getNoTelpPasien() {
        return noTelpPasien;
    }

    public void setNoTelpPasien(String noTelpPasien) {
        this.noTelpPasien = noTelpPasien;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public int getQueueNum() {
        return queueNum;
    }

    public void setQueueNum(int queueNum) {
        this.queueNum = queueNum;
    }

    public boolean isVisit() {
        return visit;
    }

    public void setVisit(boolean visit) {
        this.visit = visit;
    }

    @Override
    public String toString() {
        return "JadwalTindakan{" +
                ", namaPasien='" + namaPasien + '\'' +
                ", tindakan='" + tindakan + '\'' +
                ", operator='" + operator + '\'' +
                ", tanggal='" + tanggal.getDayOfWeek().toString() + '\'' +
                '}';
    }
}
