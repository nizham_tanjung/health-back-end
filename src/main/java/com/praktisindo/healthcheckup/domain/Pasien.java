package com.praktisindo.healthcheckup.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Pasien implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String namaPasien;

    private String pangkat;

    private String alamat;

    private String telpon1;

    @Column(unique = true)
    private String telpon2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getPangkat() {
        return pangkat;
    }

    public void setPangkat(String pangkat) {
        this.pangkat = pangkat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelpon1() {
        return telpon1;
    }

    public void setTelpon1(String telpon1) {
        this.telpon1 = telpon1;
    }

    public String getTelpon2() {
        return telpon2;
    }

    public void setTelpon2(String telpon2) {
        this.telpon2 = telpon2;
    }

    @Override
    public String toString() {
        return "Pasien{" +
                "namaPasien='" + namaPasien + '\'' +
                ", pangkat='" + pangkat + '\'' +
                ", telpon1='" + telpon1 + '\'' +
                '}';
    }
}
