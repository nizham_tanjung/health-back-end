package com.praktisindo.healthcheckup.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class TindakanPadaPasien implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate tanggal;

    private Long noRM;

    private Long pasien;

    private Long tindakan;

    private String tenagaMedis;

    private String sumberDayaDigunakan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Long getNoRM() {
        return noRM;
    }

    public void setNoRM(Long noRM) {
        this.noRM = noRM;
    }

    public Long getPasien() {
        return pasien;
    }

    public void setPasien(Long pasien) {
        this.pasien = pasien;
    }

    public Long getTindakan() {
        return tindakan;
    }

    public void setTindakan(Long tindakan) {
        this.tindakan = tindakan;
    }

    public String getTenagaMedis() {
        return tenagaMedis;
    }

    public void setTenagaMedis(String tenagaMedis) {
        this.tenagaMedis = tenagaMedis;
    }

    public String getSumberDayaDigunakan() {
        return sumberDayaDigunakan;
    }

    public void setSumberDayaDigunakan(String sumberDayaDigunakan) {
        this.sumberDayaDigunakan = sumberDayaDigunakan;
    }
}
