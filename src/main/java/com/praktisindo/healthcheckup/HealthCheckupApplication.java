package com.praktisindo.healthcheckup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthCheckupApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthCheckupApplication.class, args);
	}

}
